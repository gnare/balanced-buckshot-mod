"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Mod {
    constructor() {
    }

    postDBLoad(container) {
        const Logger = container.resolve("WinstonLogger");
        Logger.info("Running Better Buckshot patch!");
        // get database from server
        const databaseServer = container.resolve("DatabaseServer");
        // Get all the in-memory json found in /assets/database
        const tables = databaseServer.getTables();
        
        const items = tables.templates.items;

        // 12ga 5.25 mm buckshot
        items["5d6e6772a4b936088465b17c"]._props.buckshotBullets = 28;
        items["5d6e6772a4b936088465b17c"]._props.ProjectileCount = 28;
        items["5d6e6772a4b936088465b17c"]._props.Damage = 35;
        items["5d6e6772a4b936088465b17c"]._props.PenetrationPower = 1;
        items["5d6e6772a4b936088465b17c"]._props.ArmorDamage = 5;

        // 12ga 6.5 mm express buckshot
        items["5d6e67fba4b9361bc73bc779"]._props.buckshotBullets = 21;
        items["5d6e67fba4b9361bc73bc779"]._props.ProjectileCount = 21;
        items["5d6e67fba4b9361bc73bc779"]._props.Damage = 39;
        items["5d6e67fba4b9361bc73bc779"]._props.PenetrationPower = 3;
        items["5d6e67fba4b9361bc73bc779"]._props.ArmorDamage = 7;

        // 12ga 7 mm buckshot
        items["560d5e524bdc2d25448b4571"]._props.buckshotBullets = 14;
        items["560d5e524bdc2d25448b4571"]._props.ProjectileCount = 14;
        items["560d5e524bdc2d25448b4571"]._props.Damage = 44;
        items["560d5e524bdc2d25448b4571"]._props.PenetrationPower = 3;
        items["560d5e524bdc2d25448b4571"]._props.ArmorDamage = 9;

        // 12ga 8.5 mm magnum buckshot
        items["5d6e6806a4b936088465b17e"]._props.buckshotBullets = 8;
        items["5d6e6806a4b936088465b17e"]._props.ProjectileCount = 8;
        items["5d6e6806a4b936088465b17e"]._props.Damage = 55;
        items["5d6e6806a4b936088465b17e"]._props.PenetrationPower = 6;
        items["5d6e6806a4b936088465b17e"]._props.ArmorDamage = 18;

        // 12ga flechette
        items["5d6e6911a4b9361bd5780d52"]._props.buckshotBullets = 19;
        items["5d6e6911a4b9361bd5780d52"]._props.ProjectileCount = 19;
        items["5d6e6911a4b9361bd5780d52"]._props.Damage = 22;
        items["5d6e6911a4b9361bd5780d52"]._props.PenetrationPower = 31;
        items["5d6e6911a4b9361bd5780d52"]._props.ArmorDamage = 26;

        // 20ga 5.6mm buckshot
        items["5d6e695fa4b936359b35d852"]._props.buckshotBullets = 20;
        items["5d6e695fa4b936359b35d852"]._props.ProjectileCount = 20;
        items["5d6e695fa4b936359b35d852"]._props.Damage = 30;
        items["5d6e695fa4b936359b35d852"]._props.PenetrationPower = 1;
        items["5d6e695fa4b936359b35d852"]._props.ArmorDamage = 5;

        // 20ga 6.2mm buckshot
        items["5d6e69b9a4b9361bc8618958"]._props.buckshotBullets = 15;
        items["5d6e69b9a4b9361bc8618958"]._props.ProjectileCount = 15;
        items["5d6e69b9a4b9361bc8618958"]._props.Damage = 33;
        items["5d6e69b9a4b9361bc8618958"]._props.PenetrationPower = 2;
        items["5d6e69b9a4b9361bc8618958"]._props.ArmorDamage = 6;

        // 20ga 7.3mm buckshot
        items["5d6e69c7a4b9360b6c0d54e4"]._props.buckshotBullets = 10;
        items["5d6e69c7a4b9360b6c0d54e4"]._props.ProjectileCount = 10;
        items["5d6e69c7a4b9360b6c0d54e4"]._props.Damage = 36;
        items["5d6e69c7a4b9360b6c0d54e4"]._props.PenetrationPower = 3;
        items["5d6e69c7a4b9360b6c0d54e4"]._props.ArmorDamage = 9;

        // 20ga 7.5mm buckshot
        items["5a38ebd9c4a282000d722a5b"]._props.buckshotBullets = 9;
        items["5a38ebd9c4a282000d722a5b"]._props.ProjectileCount = 9;
        items["5a38ebd9c4a282000d722a5b"]._props.Damage = 38;
        items["5a38ebd9c4a282000d722a5b"]._props.PenetrationPower = 3;
        items["5a38ebd9c4a282000d722a5b"]._props.ArmorDamage = 10;

    }
}

module.exports = { mod: new Mod() };
